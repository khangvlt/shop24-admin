import React from "react";
import { Row, Col, Label } from 'reactstrap'
import { MenuItem, Select, TextField, Button, Snackbar, Alert } from "@mui/material"
import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom'
import axios from "axios";
const CreateCustomer = () => {
  let navigate = useNavigate()
  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const setAlert = (paramModal, paramAlert) => {
    setOpenAlert(true)
    setStatusModal(paramModal)
    setNoidungAlertValid(paramAlert);
  }
  // Các trường input
  const [tenDangNhap, setTenDangNhap] = useState(null)
  const [hoTen, setHoTen] = useState(null)
  const [soDienThoai, setSoDienThoai] = useState(null)
  const [email, setEmail] = useState(null)
  const [diaChi, setDiaChi] = useState(null)
  const [city, setCity] = useState(null)
  const [district, setDistrict] = useState(null)

  const [disabledDistrict, setDisableDistrict] = useState(true)
  // Mảng chứa dữ liệu trả về để kiểm tra có trùng dữ liệu trước khi đăng ký không
  const [arrTenDangNhap, setArrUserName] = useState([]);
  const [arrSoDienThoai, setArrSoDienThoai] = useState([])
  const [arrEmail, setArrEmail] = useState([])

  let d = new Date()
  let day = d.getDate();
  let month = d.getMonth() + 1;
  let year = d.getFullYear();

  const onChangeTenDangNhap = (event) => {
    setTenDangNhap(event.target.value)
  }

  const onChangeHoTen = (event) => {
    setHoTen(event.target.value)
  }

  const onChangeSoDienThoai = (event) => {
    setSoDienThoai(event.target.value)
  }

  const onChangeEmail = (event) => {
    setEmail(event.target.value)
  }

  const onChangeDiaChi = (event) => {
    setDiaChi(event.target.value)
  }

  const onChangeCity = (event) => {
    setCity(event.target.value)
    setDisableDistrict(false)
  }

  const onChangeDistrict = (event) => {
    setDistrict(event.target.value)
  }

  const validateData = () => {
    if (!hoTen) {
      setAlert('error', "Bạn chưa nhập họ tên")
      return false
    }
    if (!isNaN(hoTen)) {
      setAlert('error', 'Tên không được chứa số');
      return false;
    }
    if (email) {
      if (checkEmail(email) == false) {
        setAlert('error', 'Email sai định dạng');
        return false;
      }
      else {
        if (arrEmail.includes(email)) {
          setAlert('error', 'Email đã có')
          return false
        }
      }
    }
    if (!tenDangNhap) {
      setAlert('error', 'Bạn chưa nhập tên đăng nhập')
      return false
    }
    if (tenDangNhap.length < 8) {
      setAlert('error', 'Tên đăng nhập phải lớn hơn 8 ký tự')
      return false
    }
    if (arrTenDangNhap.includes(tenDangNhap)) {
      setAlert('error', 'Tên đăng nhập đã có');
      return false
    }
    if (validatePhoneNumber(soDienThoai) == false) {
      setAlert('error', 'Bạn phải nhập số điện thoại');
      return false;
    }
    if (arrSoDienThoai.includes(soDienThoai)) {
      setAlert('error', "Số điện thoại đã có");
      return false
    }
    if (!diaChi) {
      setAlert('error', "Bạn chưa nhập địa chỉ")
      return false
    }
    if (!city) {
      setAlert('error', "Bạn chưa chọn thành phố")
      return false
    }
    if (!district) {
      setAlert('error', "Bạn chưa chọn quận")
      return false
    }
    return true
  }

  const checkEmail = (paramEmail) => {
    const vRE = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return vRE.test(String(paramEmail).toLowerCase());
  }

  const validatePhoneNumber = (paramPhone) => {
    // Kiểu số điện thoại
    var vTypePhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    // Test chuỗi nhập vào
    var vCheck = vTypePhone.test(paramPhone);

    return vCheck;
  }

  const restApiCheckData = () => {
    axios.get(`http://computer-tech-be.herokuapp.com/customers/checkdata`)
      .then((data) => {
        for (let i in data.data.customer) {
          let tenDangNhap = data.data.customer[i].Username
          let soDienThoai = data.data.customer[i].PhoneNumber
          let email = data.data.customer[i].Email

          arrEmail.push(email)
          arrTenDangNhap.push(tenDangNhap)
          arrSoDienThoai.push(soDienThoai)

        }
      })
      .catch((error) => {
        console.log(error.response);
      })
  }

  const restApiCreateUser = () => {
    const body = {
      body: {
        FullName: hoTen,
        PhoneNumber: soDienThoai,
        Email: email,
        Address: diaChi,
        Username: tenDangNhap,
        Password: '12345678', // mặc định mật khẩu
        District: district,
        City: city,
        TimeCreated: `${day}/${month}/${year}`,
        TimeUpdated: `${day}/${month}/${year}`
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.post('http://computer-tech-be.herokuapp.com/customers', body.body, body.headers)
      .then(() => {
        setAlert('success', 'Đăng ký thành công')
        document.getElementById('hoten').value = ''
        document.getElementById('email').value = ''
        document.getElementById('username').value = ''
        document.getElementById('phone').value = ''
        document.getElementById('address').value = ''
        document.getElementById('city').value = ''
        document.getElementById('district').value = ''
      })
      .catch(error => {
        setAlert('error', 'Đăng ký thất bại!!!');
        console.log(error.response);
      })
  }

  const onClickSignup = () => {
    let valiData = validateData()
    if (valiData) {
      restApiCreateUser()
    }
  }
  useEffect(() => {
    restApiCheckData();
  }, [])
  return (
    <>
      <Row className='mt-2 p-2 text-center'>
        <h4>Create Customer</h4>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Họ tên</Label>
            </Col>
            <Col xs='8'>
              <TextField variant="standard" id='hoten' onChange={onChangeHoTen}></TextField>
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Email</Label>
            </Col>
            <Col xs='8'>
              <TextField variant="standard" id='email' onChange={onChangeEmail} ></TextField>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Username</Label>
            </Col>
            <Col xs='8'>
              <TextField variant="standard" id='username' onChange={onChangeTenDangNhap}></TextField>
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Số điện thoại</Label>
            </Col>
            <Col xs='8'>
              <TextField variant="standard" id='phone' onChange={onChangeSoDienThoai} ></TextField>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Địa chỉ</Label>
            </Col>
            <Col xs='8'>
              <TextField variant="standard" id='address' onChange={onChangeDiaChi} ></TextField>
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Thành phố</Label>
            </Col>
            <Col xs='8'>
              <Select value={city} id='city' variant="standard" onChange={onChangeCity}>
                <MenuItem value=''></MenuItem>
                <MenuItem value='tphcm'>TP. Hồ Chí Minh</MenuItem>
              </Select>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Quận</Label>
            </Col>
            <Col xs='8'>
              <Select onChange={onChangeDistrict} id='district' variant="standard" disabled={disabledDistrict} value={district}>
                <MenuItem value=''></MenuItem>
                <MenuItem value='quan1'>Quận 1</MenuItem>
                <MenuItem value='quan2'>Quận 2</MenuItem>
                <MenuItem value='quan3'>Quận 3</MenuItem>
                <MenuItem value='quan4'>Quận 4</MenuItem>
                <MenuItem value='quan5'>Quận 5</MenuItem>
                <MenuItem value='quan6'>Quận 6</MenuItem>
                <MenuItem value='quan7'>Quận 7</MenuItem>
                <MenuItem value='quan8'>Quận 8</MenuItem>
                <MenuItem value='quan9'>Quận 9</MenuItem>
                <MenuItem value='quan10'>Quận 10</MenuItem>
                <MenuItem value='quan11'>Quận 11</MenuItem>
                <MenuItem value='quan12'>Quận 12</MenuItem>
                <MenuItem value='quanBinhThanh'>Quận Bình Thạnh</MenuItem>
                <MenuItem value='quanBinhTan'>Quận Bình Tân</MenuItem>
                <MenuItem value='quanTanPhu'>Quận Tân Phú</MenuItem>
                <MenuItem value='quanGoVap'>Quận Gò Vấp</MenuItem>
                <MenuItem value='quanThuDuc'>Quận Thủ Đức</MenuItem>
                <MenuItem value='quanPhuNhuan'>Quận Phú Nhuận</MenuItem>
                <MenuItem value='huyenCuChi'>Huyện Củ Chi</MenuItem>
                <MenuItem value='huyenHocMon'>Huyện Hóc Môn</MenuItem>
                <MenuItem value='huyenBinhChanh'>Huyện Bình Chánh</MenuItem>
                <MenuItem value='huyenNhaBe'>Huyện Nhà Bè</MenuItem>
                <MenuItem value='huyenCanGio'>Huyện Cần Giờ</MenuItem>
              </Select>
            </Col>
          </Row>
        </Col>
        <Col>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Button variant="contained" onClick={onClickSignup}>Thêm người dùng</Button>
      </Row>
      <Snackbar open={openAlert} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default CreateCustomer
