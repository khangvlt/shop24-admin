import axios from "axios"
import { useEffect, useState, React } from "react"
import { Link } from "react-router-dom";
import { Button, Row } from "reactstrap"
import DataCustomers from "./DataCustomers";
const Customers = () => {

  const [aRRCustomer, setArrCustomer] = useState([])

  const restApiGetAllCustomers = () => {
    axios.get('http://computer-tech-be.herokuapp.com/customers')
      .then((data) => {
        setArrCustomer(data.data.customer)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  useEffect(() => {
    restApiGetAllCustomers()
  }, [])
  return (
    <>
      <Row className="mt-2 p-2 text-center">
        <h2>Khách hàng</h2>
      </Row>
      <Row className="mt-2 p-2">
        <Link to={'/customers/create'}>
        <Button className="btn-success" style={{ width:200 }}>Create new</Button>
        </Link>
      </Row>
      <Row className="mt-2 p-2">
        <DataCustomers aRRCustomer={aRRCustomer} restApiGetAllCustomers={restApiGetAllCustomers} />
      </Row>
    </>
  )
}
export default Customers
