import { Button, Snackbar, Alert } from "@mui/material"
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap"
import axios from 'axios'
import { React, useState } from "react"

const ModalUpdateStatus = ({phoneCustomer, restApiGetOrderByPhone, openModalUpdateStatus, setOpenModalUpdateStatus, orderId, status, restApiGetAllOrders }) => {

  let d = new Date()
  let day = d.getDate();
  let month = d.getMonth() + 1;
  let year = d.getFullYear();

  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const setAlert = (paramModal, paramAlert) => {
    setOpenAlert(true)
    setStatusModal(paramModal)
    setNoidungAlertValid(paramAlert);
  }

  const restApiUpdateStatusOrder = () => {
    const body = {
      body: {
        Status: status,
        TimeUpdated: `${day}/${month}/${year}`
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.put(`http://computer-tech-be.herokuapp.com/orders/${orderId}`, body.body, body.headers)
      .then(() => {
        setAlert('success', 'Cập nhật thành công')
        if(phoneCustomer) {
          restApiGetOrderByPhone(phoneCustomer)
        }
        else {
          restApiGetAllOrders()
        }
      })
      .catch(error => {
        console.log(error.response);
        setAlert('error', 'Cập nhật thất bại')
      })
  }
  const onClickConfirm = () => {
    restApiUpdateStatusOrder()
  }
  return (
    <>
      <Modal isOpen={openModalUpdateStatus} toggle={() => setOpenModalUpdateStatus(false)}>
        <ModalHeader>Cập nhật trạng thái đơn hàng</ModalHeader>
        <ModalBody>
          Cập nhật đơn hàng <b>{orderId}</b> <h4 style={{ color: 'red' }}>{status}</h4>
        </ModalBody>
        <ModalFooter>
          <Button variant="contained" onClick={onClickConfirm}>Confirm</Button>
        </ModalFooter>
      </Modal>

      <Snackbar open={openAlert} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal} >
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalUpdateStatus
