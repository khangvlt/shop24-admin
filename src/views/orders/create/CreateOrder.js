import { CCard, CCardBody, CCardFooter, CCardHeader } from '@coreui/react'
import { TextField, Button, Select, MenuItem, Snackbar, Alert } from '@mui/material'
import axios from 'axios'
import { FaTrashAlt } from "react-icons/fa";
import React, { useState } from 'react'
import { Row, Col, Label } from 'reactstrap'
const CreateOrder = () => {
  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const setAlert = (paramModal, paramAlert) => {
    setOpenAlert(true)
    setStatusModal(paramModal)
    setNoidungAlertValid(paramAlert);
  }
  const [aRRNameProduct, setArrNameProduct] = useState([])
  const [objProduct, setObjProduct] = useState([])
  const [objCustomer, setObjCustomer] = useState([])
  const [amount, setAmount] = useState(1)
  const [phoneNumber, setPhoneNumber] = useState(null)
  const [displayCard, setDisplayCard] = useState('none')
  const [aRRCart, setArrCart] = useState([])

  let sumPrice = 0
  let sumAmount = 0
  let priceProduct = 0

  for (let i in aRRCart) {
    priceProduct = aRRCart[i].price * aRRCart[i].amount
    sumAmount += aRRCart[i].amount
    sumPrice += priceProduct
  }

  // Ngày tháng năm
  let d = new Date()
  let day = d.getDate();
  let month = d.getMonth() + 1;
  let year = d.getFullYear();
  let toDay = `${day}/${month}/${year}`
  let shippedDay = `${day + 5}/${month}/${year}`;

  const restApiGetCustomerByPhone = (paramPhone) => {
    axios.get(`http://computer-tech-be.herokuapp.com/customers/getIdByPhone/${paramPhone}`)
      .then((data) => {
        setObjCustomer(data.data.customer)
        if (data.data.customer.length > 0) {
          setDisplayCard('block')
          getCartByIdUser(data.data.customer[0]._id)
        }
        else if (data.data.customer.length == 0) {
          setAlert('error', 'Không có người dùng này')
          setDisplayCard('none')
        }
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const restApiGetProductByType = (paramType) => {
    axios.get(`http://computer-tech-be.herokuapp.com/products/type/${paramType}`)
      .then((data) => {
        setArrNameProduct(data.data.product)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const restApiGetProductById = (paramProductId) => {
    axios.get(`http://computer-tech-be.herokuapp.com/products/${paramProductId}`)
      .then((data) => {
        setObjProduct(data.data.product)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const onChangeType = (event) => {
    let type = event.target.value
    restApiGetProductByType(type)
  }

  const onChangeSelectProduct = (event) => {
    restApiGetProductById(event.target.value)
  }

  const onChangePhone = (event) => {
    setPhoneNumber(event.target.value)
  }

  const onClickCheckPhone = () => {
    let validata = validateInput(phoneNumber)
    if (validata) {
      restApiGetCustomerByPhone(phoneNumber)
    }
  }

  const onChangeAmount = (event) => {
    setAmount(event.target.value)

  }

  const restApiForCreateCart = () => {
    const body = {
      body: {
        productId: objProduct._id,
        amount: amount,
        price: objProduct.PromotionPrice,
        name: objProduct.Name,
        imgAvatar: objProduct.ImageUrl,
        isChecked: true
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.post(`http://computer-tech-be.herokuapp.com/customers/${objCustomer[0]._id}/${objProduct._id}/cart`, body.body, body.headers)
      .then(() => {
        getCartByIdUser(objCustomer[0]._id)
        document.getElementById('inputAmount').value = ''
      })
      .catch(error => {
        //toast.error('Thêm vào giỏ thất bại!!!');
        console.log(error.response);
      })
  }

  const getCartByIdUser = async (paramCustomerId) => {
    axios.get(`http://computer-tech-be.herokuapp.com/customers/${paramCustomerId}/cart`)
      .then((data) => {
        setArrCart(data.data.Carts)
      })
      .catch((error) => {
        console.log(error.response);
      })
  }

  const restApiForUpdateCart = (paramCart) => {
    const body = {
      body: {
        amount: amount,
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.put(`http://computer-tech-be.herokuapp.com/cart/${paramCart._id}`, body.body, body.headers)
      .then(() => {
        getCartByIdUser(objCustomer[0]._id)
        document.getElementById('inputAmount').value = ''
      })
      .catch(error => {
        //toast.error('cập nhật thất bại!!!');
        console.log(error.response);
      })
  }

  const onClickDeleteItem = (paramID) => {
    axios.delete(`http://computer-tech-be.herokuapp.com/customers/${objCustomer[0]._id}/${paramID}/cart`)
      .then(() => {
        getCartByIdUser(objCustomer[0]._id)
      })
      .catch((error) => {
        console.log(error.response)
      })
  }

  var checkIdProduct = false

  const onClickAddToCart = () => {

    let validata = validateInput(amount)
    if (aRRNameProduct.length != 0 && objProduct.length != 0) {
      if (validata) {
        for (let i in aRRCart) {
          if (aRRCart[i].productId === objProduct._id) {
            checkIdProduct = true
            restApiForUpdateCart(aRRCart[i])
          }
        }
        if (checkIdProduct == false) {
          restApiForCreateCart()
        }
      }
    }
    else {
      setAlert('error', 'Bạn phải chọn sản phẩm')
    }
  }

  const restApiCreateOrder = () => {
    const body = {
      body: {
        OrderDate: toDay,
        RequiredDate: toDay,
        TimeCreated: toDay,
        TimeUpdated: toDay,
        ShippedDate: shippedDay,
        Note: `Đơn hàng có ${sumAmount} sản phẩm`,
        Status: `Open`,
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.post(`http://computer-tech-be.herokuapp.com/customers/${objCustomer[0]._id}/orders`, body.body, body.headers)
      .then((data) => {
        /*  - Nếu userId đó chưa có đơn hàng nào thì khi tạo lúc này sẽ có 1 đơn hàng,
                    ta sẽ dùng orderId đó thực hiện tiếp
            - Nêu userId đó đã có đơn thì lấy mã đơn cuối là lúc rest API tạo ra*/
        let lastOrderId = data.data.order.Orders.length - 1
        if (data.data.order.Orders.length === 1) {
          for (let i in aRRCart) {
            restAPIGetQuantityProductById(aRRCart[i], data.data.order.Orders[0])
          }
        }
        else {
          for (let i in aRRCart) {
            restAPIGetQuantityProductById(aRRCart[i], data.data.order.Orders[lastOrderId])
          }
        }
      })
      .catch(error => {
        console.log(error.response);
      })
  }

  const restApiCreateOrderDetail = (param, paramOrderId) => {
    const body = {
      body: {
        Name: param.name,
        ImgUrl: param.imgAvatar,
        Quantity: param.amount,
        PriceEach: param.price * param.amount
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.post(`http://computer-tech-be.herokuapp.com/orders/${paramOrderId}/${param.productId}/orderDetail`, body.body, body.headers)
      .then(() => {
        setAlert('success', 'Tạo đơn thành công')
      })
      .catch(error => {
        console.log(error.response);
      })
  }

  const restApiUpdateQuantity = (param, paramQuantiy) => {
    const body = {
      body: {
        Quantity: paramQuantiy - param.amount,
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.put(`http://computer-tech-be.herokuapp.com/products/${param.productId}`, body.body, body.headers)
      .then(() => {
        //console.log('OK')
      })
      .catch(error => {
        console.log(error.response);
      })
  }

  const restApiDeleteCartById = (paramID) => {
    axios.delete(`http://computer-tech-be.herokuapp.com/customers/${objCustomer[0]._id}/${paramID._id}/cart`)
      .then(() => {
        getCartByIdUser(objCustomer[0]._id)
      })
      .catch((error) => {
        console.log(error.response)
      })
  }

  const restAPIGetQuantityProductById = (param, paramOrderId) => {
    axios.get(`http://computer-tech-be.herokuapp.com/products/${param.productId}/quantity`)
      .then((data) => {
        if (param.amount > data.data.product.Quantity) {
          setAlert('error', 'Không đủ số lượng sản phẩm')
        }
        else {
          restApiCreateOrderDetail(param, paramOrderId)
          restApiDeleteCartById(param)
          restApiUpdateQuantity(param, data.data.product.Quantity)

        }
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const validateInput = (paramInput) => {
    if (isNaN(paramInput)) {
      setAlert('error', 'Bạn phải nhập số')
      return false
    }
    return true
  }

  const onClickCreateOrder = () => {
    if (aRRCart.length > 0) {
      restApiCreateOrder()
    }
    else {
      setAlert('error', 'Bạn chưa chọn sản phẩm')
    }
  }
  return (
    <>
      <Row className='mt-2 p-2 text-center'>
        <h3>Create Order</h3>
      </Row>
      <Row className='mt-2 p-2'>
        <Col xs='3'>
          <TextField onChange={onChangePhone} placeholder='Phone number' variant='standard' /></Col>
        <Col xs='3'>
          <Button variant='contained' onClick={onClickCheckPhone}>Check</Button></Col>
      </Row>
      <Row className='mt-2 p-2'>
        <CCard className='mt-4' style={{ display: displayCard }}>
          <CCardHeader>
            {
              objCustomer.length > 0 ?
                "Tên: " + objCustomer[0].FullName + ' | Địa chỉ: ' + objCustomer[0].Address + ' ' + objCustomer[0].District + ' ' + objCustomer[0].City
                : null
            }
          </CCardHeader>
          <CCardBody>
            <Row>
              <Col xs='6'>
                <Row>
                  <Col xs='2'>
                    <Label>Loại</Label>
                  </Col>
                  <Col xs='5'>
                    <Select variant='standard' onChange={onChangeType}>
                      <MenuItem value='keyboard'>Bàn phím</MenuItem>
                      <MenuItem value='mouse'>Chuột</MenuItem>
                      <MenuItem value='monitor'>Màn hình</MenuItem>
                      <MenuItem value='vga'>VGA</MenuItem>
                      <MenuItem value='ssd'>SSD</MenuItem>
                      <MenuItem value='headphone'>Tai nghe</MenuItem>
                      <MenuItem value='case'>Case</MenuItem>
                      <MenuItem value='mainboard'>Mainboard</MenuItem>
                    </Select>
                  </Col>
                </Row>
                <Row className='mt-4'>
                  <Col xs='2'>
                    <Label>Sản phẩm</Label>
                  </Col>
                  <Col xs='5'>
                    <Select variant='standard' style={{ maxWidth: 100 }} onChange={onChangeSelectProduct}>
                      {
                        aRRNameProduct.length > 0 ?
                          aRRNameProduct.map((item, index) => {
                            return (
                              <MenuItem value={item._id} key={index}>{item.Name}</MenuItem>
                            )
                          })
                          : null
                      }
                    </Select>
                  </Col>
                </Row>
                <Row className='mt-4'>
                  <Col xs='2'>
                    <Label>Số lượng</Label>
                  </Col>
                  <Col xs='5'>
                    <TextField id='inputAmount' onChange={onChangeAmount} placeholder='Nhập số lượng' variant='standard' />
                  </Col>
                </Row>
                <Row className='mt-4'>
                  <Col xs='2'>
                  </Col>
                  <Col xs='5'>
                    <Button variant='contained' onClick={onClickAddToCart}>Thêm</Button>
                  </Col>
                </Row>
              </Col>
              <Col xs='6' style={{ borderLeft: '1px solid black' }}>
                {
                  aRRCart.length > 0 ?
                    aRRCart.map((item, index) => {
                      return (
                        <Row style={{ marginLeft: 20 }} key={index}>
                          <Col>{index + 1}. : {item.name}, số lượng: {item.amount}</Col>
                          <Col xs='2'>  <FaTrashAlt onClick={() => onClickDeleteItem(item._id)} className='styleIcon' style={{ color: 'red' }} />
                          </Col>
                        </Row>
                      )
                    }) : null
                }
              </Col>
            </Row>
          </CCardBody>
          <CCardFooter>
            <Button variant='contained' onClick={onClickCreateOrder}>Tạo đơn hàng</Button>
          </CCardFooter>
        </CCard>
      </Row>
      <Snackbar open={openAlert} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default CreateOrder
