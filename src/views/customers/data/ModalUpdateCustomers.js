import { Modal, ModalBody, ModalHeader, Row, Label, Col, ModalFooter } from "reactstrap"
import { MenuItem, Select, TextField, Button, Snackbar, Alert } from "@mui/material"
import axios from "axios"
import { useEffect, useState, React } from "react"
const ModalUpdateCustomer = ({ openModalUpdateCustomer, setOpenModalUpdateCustomer, customerId, restApiGetAllCustomers }) => {
  const [name, setName] = useState(null)
  const [email, setEmail] = useState(null)
  const [phone, setPhone] = useState(null)
  const [city, setCity] = useState(null)
  const [district, setDistrict] = useState(null)
  const [address, setAddress] = useState(null)

  const [disableEmail, setDisableEmail] = useState(false)
  const [aRREmail, setArrEmail] = useState([])
  const [aRRPhone, setArrPhone] = useState([])
  const [objUser, setObjUser] = useState({})

  let d = new Date()
  let day = d.getDate();
  let month = d.getMonth() + 1;
  let year = d.getFullYear();


  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const setAlert = (paramModal, paramAlert) => {
    setOpenAlert(true)
    setStatusModal(paramModal)
    setNoidungAlertValid(paramAlert);
  }

  const restApiGetCustomerById = () => {
    axios.get(`http://computer-tech-be.herokuapp.com/customers/${customerId}`)
      .then((data) => {
        setObjUser(data.data.customer)
        setName(data.data.customer.FullName)
        setEmail(data.data.customer.Email)
        setPhone(data.data.customer.PhoneNumber)
        setCity(data.data.customer.City)
        setDistrict(data.data.customer.District)
        setAddress(data.data.customer.Address)
        if (data.data.customer.Username === "") {
          setDisableEmail(true)
        }
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const restApiCheckData = () => {
    axios.get(`http://computer-tech-be.herokuapp.com/customers/checkdata`)
      .then((data) => {
        for (let i in data.data.customer) {
          let soDienThoai = data.data.customer[i].PhoneNumber
          let email = data.data.customer[i].Email

          aRREmail.push(email)
          aRRPhone.push(soDienThoai)
        }
      })
      .catch((error) => {
        console.log(error.response);
      })
  }

  const onChangePhone = (event) => {
    setPhone(event.target.value)
  }

  const onChangeAddress = (event) => {
    setAddress(event.target.value)
  }

  const onChangeDistrict = (event) => {
    setDistrict(event.target.value)
  }

  const onChangeEmail = (event) => {
    setEmail(event.target.value)
  }

  const checkEmail = (paramEmail) => {
    const vRE = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return vRE.test(String(paramEmail).toLowerCase());
  }

  const validatePhoneNumber = (paramPhone) => {
    // Kiểu số điện thoại
    var vTypePhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    // Test chuỗi nhập vào
    var vCheck = vTypePhone.test(paramPhone);

    return vCheck;
  }

  const validate = () => {
    // Nếu đăng ký bình thường thì cho sửa email
    if (objUser.Email !== email) {
      if (email) {
        if (checkEmail(email) == false) {
          setAlert('error', 'Email sai định dạng')
          return false;
        }
        else {
          if (aRREmail.includes(email)) {
            setAlert('error', 'Email đã có')
            return false
          }
        }
      }
    }

    if (validatePhoneNumber(phone) == false) {
      setAlert('error', 'Bạn phải nhập số điện thoại');
      return false;
    }
    if (!address) {
      setAlert('error', "Bạn chưa nhập địa chỉ")
      return false
    }
    if (!district) {
      setAlert('error', "Bạn chưa chọn quận")
      return false
    }
    return true
  }

  const restApiUpdateUser = () => {
    const body = {
      body: {
        PhoneNumber: phone,
        Address: address,
        City: city,
        District: district,
        Email: email,
        TimeUpdated: `${day}/${month}/${year}`
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.put(`http://computer-tech-be.herokuapp.com/customers/${customerId}`, body.body, body.headers)
      .then(() => {
        setAlert('success', 'Cập nhật thành công');
        restApiGetAllCustomers()
      })
      .catch(error => {
        setAlert('error', 'Cập nhật thất bại!!!');
        console.log(error.response);
      })
  }

  const onClickUpdateCustomer = () => {
    let valiData = validate()
    // TH1 : Ko đổi số điện thoại và đổi địa chỉ
    if (objUser.PhoneNumber === phone && (objUser.District !== district || objUser.Address !== address || objUser.Email !== email)) {
      if (valiData) {
        restApiUpdateUser()
      }
    }
    // TH2 : Đổi sđt và đổi địa chỉ..
    else if (objUser.PhoneNumber !== phone && (objUser.District !== district || objUser.Address !== address || objUser.Email !== email)) {
      if (valiData) {
        if (aRRPhone.includes(phone)) {
          setAlert('error', "Số điện thoại đã có");
        }
        else {
          restApiUpdateUser()
        }
      }
    }
    // TH3 : Chỉ Đổi Sđt
    else if (objUser.PhoneNumber !== phone && (objUser.District === district || objUser.Address === address || objUser.Email === email)) {
      if (valiData) {
        if (aRRPhone.includes(phone)) {
          setAlert('error', "Số điện thoại đã có");
        }
        else {
          restApiUpdateUser()
        }
      }
    }
  }

  useEffect(() => {
    restApiCheckData()
    restApiGetCustomerById()
    setEmail(null)
    setDisableEmail(false)
  }, [customerId])
  return (
    <>
      <Modal isOpen={openModalUpdateCustomer} toggle={() => setOpenModalUpdateCustomer(false)} size='lg'>
        <ModalHeader>Cập nhật khách hàng</ModalHeader>
        <ModalBody>
          <Row>
            {
              disableEmail === true ? <pre>(*) Khách hàng đăng ký bằng Google</pre> : null
            }
          </Row>
          <Row className="p-2 mt-2">
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Họ Tên</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" disabled={true} value={name} style={{ minWidth: 200 }}></TextField></Col>
              </Row>
            </Col>
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Email</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" onChange={onChangeEmail} disabled={disableEmail} value={email} style={{ minWidth: 200 }}></TextField></Col>
              </Row>
            </Col>
          </Row>
          <Row className="p-2 mt-2">
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Số điện thoại</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" onChange={onChangePhone} value={phone} style={{ minWidth: 200 }}></TextField></Col>
              </Row>
            </Col>
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Địa chỉ</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" onChange={onChangeAddress} value={address} style={{ minWidth: 200 }}></TextField></Col>
              </Row>
            </Col>
          </Row>
          <Row className="p-2 mt-2">
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Thành phố</Label></Col>
                <Col xs='9'>
                  <Select value={city} variant="standard" style={{ minWidth: 200 }}>
                    <MenuItem value='tphcm'>TP. Hồ Chí Minh</MenuItem>
                  </Select>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Địa chỉ</Label></Col>
                <Col xs='9'>
                  <Select style={{ minWidth: 200 }} onChange={onChangeDistrict} variant="standard" value={district}>
                    <MenuItem value='quan1'>Quận 1</MenuItem>
                    <MenuItem value='quan2'>Quận 2</MenuItem>
                    <MenuItem value='quan3'>Quận 3</MenuItem>
                    <MenuItem value='quan4'>Quận 4</MenuItem>
                    <MenuItem value='quan5'>Quận 5</MenuItem>
                    <MenuItem value='quan6'>Quận 6</MenuItem>
                    <MenuItem value='quan7'>Quận 7</MenuItem>
                    <MenuItem value='quan8'>Quận 8</MenuItem>
                    <MenuItem value='quan9'>Quận 9</MenuItem>
                    <MenuItem value='quan10'>Quận 10</MenuItem>
                    <MenuItem value='quan11'>Quận 11</MenuItem>
                    <MenuItem value='quan12'>Quận 12</MenuItem>
                    <MenuItem value='quanBinhThanh'>Quận Bình Thạnh</MenuItem>
                    <MenuItem value='quanBinhTan'>Quận Bình Tân</MenuItem>
                    <MenuItem value='quanTanPhu'>Quận Tân Phú</MenuItem>
                    <MenuItem value='quanGoVap'>Quận Gò Vấp</MenuItem>
                    <MenuItem value='quanThuDuc'>Quận Thủ Đức</MenuItem>
                    <MenuItem value='quanPhuNhuan'>Quận Phú Nhuận</MenuItem>
                    <MenuItem value='huyenCuChi'>Huyện Củ Chi</MenuItem>
                    <MenuItem value='huyenHocMon'>Huyện Hóc Môn</MenuItem>
                    <MenuItem value='huyenBinhChanh'>Huyện Bình Chánh</MenuItem>
                    <MenuItem value='huyenNhaBe'>Huyện Nhà Bè</MenuItem>
                    <MenuItem value='huyenCanGio'>Huyện Cần Giờ</MenuItem>
                  </Select>
                </Col>
              </Row>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button variant="contained" onClick={onClickUpdateCustomer}>Cập nhật</Button>
        </ModalFooter>
      </Modal>

      <Snackbar open={openAlert} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalUpdateCustomer
