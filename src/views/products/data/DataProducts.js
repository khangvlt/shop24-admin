
import { FcSupport } from "react-icons/fc";
import { FaTrashAlt } from "react-icons/fa";
import { Button, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableFooter, TableHead, TableRow } from "@mui/material"
import { useState, React } from "react";
import ModalUpdateProduct from "./ModalUpdateProduct";
import ModalDeleteProduct from "./ModalDeleteProduct";

const DataProducts = ({ aRRProduct, restAPIGetAllProducts }) => {
  // Chỉnh style cho table giỏ hàng
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const [openModalUpdate, setOpenModalUpdate] = useState(false)
  const [openModalDelete, setOpenModalDelete] = useState(false)

  const [productId, setProductId] = useState(null)
  const [nameProduct, setNameProduct] = useState(null)

  const onClickUpdate = (paramProductId) => {
    setOpenModalUpdate(true)
    setProductId(paramProductId)
  }
  const onClickDelete = (paramProductId, paramName) => {
    setOpenModalDelete(true)
    setProductId(paramProductId)
    setNameProduct(paramName)
  }
  return (
    <>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow className="bg-info">
              <TableCell width={'5%'} align="center"><b>STT</b></TableCell>
              <TableCell width={'10%'} align="center"><b>Mã SP</b></TableCell>
              <TableCell width={'15%'} align="center"><b>Tên</b></TableCell>
              <TableCell width={'10%'} align="center"><b>Loại</b></TableCell>
              <TableCell width={'25%'} align="center"><b>Hình</b></TableCell>
              <TableCell width={'10%'} align="center"><b>Giá mua</b></TableCell>
              <TableCell width={'10%'} align="center"><b>Giá giảm</b></TableCell>
              <TableCell width={'5%'} align="center"><b>Brand</b></TableCell>
              <TableCell></TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              aRRProduct.map((item, index) => {
                return (
                  <StyledTableRow key={index}>
                    <StyledTableCell align="center">{index + 1}</StyledTableCell>
                    <StyledTableCell align="center">{item._id}</StyledTableCell>
                    <StyledTableCell align="center">{item.Name}</StyledTableCell>
                    <StyledTableCell align="center">{item.Type}</StyledTableCell>
                    <StyledTableCell align="center">
                      <img src={item.ImageUrl} style={{ width: '50%' }} alt='img' />
                    </StyledTableCell>
                    <StyledTableCell align="center">{item.BuyPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</StyledTableCell>
                    <StyledTableCell align="center">{item.PromotionPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</StyledTableCell>
                    <StyledTableCell align="center">{item.Brand}</StyledTableCell>
                    <StyledTableCell>
                      <FcSupport style={{ color: 'green' }} onClick={() => onClickUpdate(item._id)} className='styleIcon' />
                    </StyledTableCell>
                    <StyledTableCell>
                      <FaTrashAlt style={{ color: 'red' }} onClick={() => onClickDelete(item._id, item.Name)} className='styleIcon' />
                    </StyledTableCell>
                  </StyledTableRow>
                )
              })
            }
          </TableBody>
        </Table>
      </TableContainer>
      <ModalUpdateProduct restAPIGetAllProducts={restAPIGetAllProducts} openModalUpdate={openModalUpdate} productId={productId} setOpenModalUpdate={setOpenModalUpdate} />
      <ModalDeleteProduct restAPIGetAllProducts={restAPIGetAllProducts} openModalDelete={openModalDelete} productId={productId} nameProduct={nameProduct} setOpenModalDelete={setOpenModalDelete} />
    </>
  )
}
export default DataProducts
