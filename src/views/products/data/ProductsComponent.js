import { Row, Col, Label, Button } from 'reactstrap'
import { useEffect, useState, React } from 'react'
import axios from 'axios'
import DataProducts from './DataProducts'
import PaginationProducts from './PaginationProducts'
import {Link} from 'react-router-dom'
import { MenuItem, Select, TextField } from '@mui/material'
function ProductsComponent() {

  const [aRRProduct, setArrProduct] = useState([])

  const [typeProduct, setTypeProduct] = useState(null)
  const [productName, setProductName] = useState(null)
  const [disabledName, setDisabledName] = useState(true)
  const [page, setPage] = useState(1);
  const [noPage, setNoPage] = useState(0)
  const limit = 6

  const restAPIGetAllProducts = () => {
    axios.get(`http://computer-tech-be.herokuapp.com/products?skip=${page}&limit=${limit}`)
      .then((data) => {
        setNoPage(data.data.pages)
        setArrProduct(data.data.products)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const restAptGetProductByType = (paramType) => {
    axios.get(`http://computer-tech-be.herokuapp.com/products/type/${paramType}`)
      .then((data) => {
        setNoPage(Math.ceil(data.data.product.length / limit))
        setArrProduct(data.data.product.slice((page - 1) * limit, page * limit))

      })
  }

  const onChangeNameProduct = (event) => {
    setProductName(event.target.value)
    restApiGetProductByNane()
  }

  const restApiGetProductByNane = () => {
    axios.get(`http://computer-tech-be.herokuapp.com/products/type/${typeProduct}/name/${productName}`)
      .then((data) => {
        setNoPage(Math.ceil(data.data.product.length / limit))
        setArrProduct(data.data.product.slice((page - 1) * limit, page * limit))
      })
  }

  const onChangeTypeProduct = (event) => {
    setPage(1)
    setDisabledName(false)
    setTypeProduct(event.target.value)
    restAptGetProductByType(event.target.value)
  }

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
    if (typeProduct !== null) {
      restAptGetProductByType(typeProduct)
    }
    else {
      restAPIGetAllProducts()
      setDisabledName(true)
    }
  }, [page, typeProduct])
  return (
    <>
      <div>
        <Row className='mt-2 p-2'>
          <h1 style={{ textAlign: 'center' }}>Danh sách sản phẩm</h1>
        </Row>
        <Row className='mt-2 p-2'>
          <Link to={'/products/create'}>
            <Button className="btn-success" style={{ width: 200 }}>Create new</Button>
          </Link>
        </Row>
        <Row className='mt-2 p-2'>
          <Col xs='3'>
            <Label>Loại </Label>
            <Select style={{ marginLeft: 10 }} variant="standard" value={typeProduct} onChange={onChangeTypeProduct}>
              <MenuItem value={null}>None</MenuItem>
              <MenuItem value='keyboard'>Bàn phím</MenuItem>
              <MenuItem value='mouse'>Chuột</MenuItem>
              <MenuItem value='monitor'>Màn hình</MenuItem>
              <MenuItem value='vga'>VGA</MenuItem>
              <MenuItem value='ssd'>SSD</MenuItem>
              <MenuItem value='headphone'>Tai nghe</MenuItem>
              <MenuItem value='case'>Case</MenuItem>
              <MenuItem value='mainboard'>Mainboard</MenuItem>
            </Select>
          </Col>
          <Col xs='4'>
            <Label>Tên SP </Label>
            <TextField style={{ marginLeft: 10 }} variant='standard' disabled={disabledName} onChange={onChangeNameProduct}></TextField>
          </Col>
        </Row>
        <Row className='mt-2 p-2'>
          <DataProducts aRRProduct={aRRProduct} restAPIGetAllProducts={restAPIGetAllProducts} />
        </Row>
        <Row className='mt-2 p-2'>
          <PaginationProducts page={page} noPage={noPage} setPage={setPage} />
        </Row>
      </div>
    </>
  )
}
export default ProductsComponent
