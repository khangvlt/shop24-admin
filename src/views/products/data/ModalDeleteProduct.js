import axios from "axios"
import {React, useState} from "react"
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap"
import { Snackbar, Alert } from '@mui/material'

const ModalDeleteProduct = ({ openModalDelete, setOpenModalDelete, productId, nameProduct, restAPIGetAllProducts }) => {

  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const setAlert = (paramModal, paramAlert) => {
    setOpenAlert(true)
    setStatusModal(paramModal)
    setNoidungAlertValid(paramAlert);
  }
  const restApiDeleteProductById = () => {
    axios.delete(`http://computer-tech-be.herokuapp.com/products/${productId}`)
      .then(() => {
        restAPIGetAllProducts()
        setAlert('success', 'Xóa thành công')
      })
      .catch((err) => {
        setAlert('error', 'Xóa thất bại')
        console.log(err.message)
      })
  }

  const onClickDeleteProduct = () => {
    restApiDeleteProductById()
  }
  return (
    <>
      <Modal isOpen={openModalDelete} toggle={() => setOpenModalDelete(false)}>
        <ModalHeader>
          Xóa sản phẩm
        </ModalHeader>
        <ModalBody>
          Bạn có thật sự muốn xóa sản phẩm <b>{nameProduct}</b> không ?
        </ModalBody>
        <ModalFooter>
          <Button onClick={onClickDeleteProduct} className="btn-danger">Confirm</Button>
        </ModalFooter>
      </Modal>
      <Snackbar open={openAlert} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalDeleteProduct
