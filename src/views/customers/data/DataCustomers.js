import {  styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableFooter, TableHead, TableRow } from "@mui/material"
import { useState, React } from "react";
import { FcSupport } from "react-icons/fc";
import ModalUpdateCustomer from './ModalUpdateCustomers';

const DataCustomers = ({ aRRCustomer, restApiGetAllCustomers }) => {
    // Chỉnh style cho table giỏ hàng
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    const [openModalUpdateCustomer, setOpenModalUpdateCustomer] = useState(false)
    const [customerId, setCustomerId] = useState(null)

    const onClickUpdate = (paramId) => {
        setOpenModalUpdateCustomer(true)
        setCustomerId(paramId)
    }
    return (
        <>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow className="bg-info">
                            <TableCell width={'5%'} align="center"><b>STT</b></TableCell>
                            <TableCell width={'20%'} align="center"><b>Họ tên</b></TableCell>
                            <TableCell width={'20%'} align="center"><b>Email</b></TableCell>
                            <TableCell width={'10%'} align="center"><b>SĐT</b></TableCell>
                            <TableCell width={'10%'} align="center"><b>Thành phố</b></TableCell>
                            <TableCell width={'10%'} align="center"><b>Quận</b></TableCell>
                            <TableCell width={'20%'} align="center"><b>Địa chỉ</b></TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            aRRCustomer.map((item, index) => {
                                return (
                                    <StyledTableRow key={index}>
                                        <StyledTableCell align="center">{index + 1}</StyledTableCell>
                                        <StyledTableCell align="center">{item.FullName}</StyledTableCell>
                                        <StyledTableCell align="center">{item.Email}</StyledTableCell>
                                        <StyledTableCell align="center">{item.PhoneNumber}</StyledTableCell>
                                        <StyledTableCell align="center">{item.City}</StyledTableCell>
                                        <StyledTableCell align="center">{item.District}</StyledTableCell>
                                        <StyledTableCell align="center">{item.Address}</StyledTableCell>
                                        <StyledTableCell>
                                            <FcSupport onClick={() => onClickUpdate(item._id)} className='styleIcon' />
                                        </StyledTableCell>
                                    </StyledTableRow>
                                )
                            })
                        }
                    </TableBody>
                </Table>
            </TableContainer>
            <ModalUpdateCustomer restApiGetAllCustomers={restApiGetAllCustomers} openModalUpdateCustomer={openModalUpdateCustomer} customerId={customerId} setOpenModalUpdateCustomer={setOpenModalUpdateCustomer}/>
        </>
    )
}
export default DataCustomers
