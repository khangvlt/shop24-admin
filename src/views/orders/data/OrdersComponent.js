import axios from "axios"
import { useEffect, useState, React } from "react"
import { Row, Button } from "reactstrap"
import { Link } from 'react-router-dom'
import DataOrders from './DataOrders'
import { Label, Col } from 'reactstrap'
import { MenuItem, Select, TextField } from '@mui/material'
function OrdersComponent() {
  const [aRROrders, setArrOrders] = useState([])
  const restApiGetAllOrders = () => {
    axios.get('http://computer-tech-be.herokuapp.com/orders')
      .then((data) => {
        setArrOrders((data.data.order))
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const [phoneCustomer, setPhoneCustomer] = useState(null)
  const [customerName, setCustomerName] = useState(null)

  const restApiGetOrderByPhone = (param) => {
    axios.get(`http://computer-tech-be.herokuapp.com/customers/getIdByPhone/${param}`)
      .then((data) => {
        restApiGetOrderByIdCustomer(data.data.customer[0]._id)
        setCustomerName(data.data.customer[0].FullName)
      })
  }

  const restApiGetOrderByIdCustomer = (paramIdCustomer) => {
    axios.get(`http://computer-tech-be.herokuapp.com/customers/${paramIdCustomer}/orders`)
      .then((data) => {
        setArrOrders((data.data.Order))
      })
  }

  const onChangePhone = (event) => {
    setPhoneCustomer(event.target.value)
  }

  const onClickSearch = () => {
    restApiGetOrderByPhone(phoneCustomer)
  }

  useEffect(() => {
    restApiGetAllOrders()
  }, [])
  return (
    <>
      <Row className="mt-2 p-2 text-center">
        <h2>Danh sách Đơn hàng</h2>
      </Row>
      <Row className="mt-2 p-2">
        <Link to={'/orders/create'}>
          <Button className="btn-success" style={{ width: 200 }}>Create new</Button>
        </Link>
      </Row>
      <Row className="mt-2 p-2">
        <Select variant="standard">
          <MenuItem>Đơn hàng</MenuItem>
        </Select>
      </Row>
      <Row className='mt-2 p-2'>
        <Col xs='5'>
          <Label>Nhập SĐT Khách hàng</Label>
          <TextField style={{ marginLeft: 10 }} variant="standard" onChange={onChangePhone}></TextField>
        </Col>
        <Col xs='3'>
          <Button className="btn-danger" onClick={onClickSearch}>Search</Button>
        </Col>
        <Col xs='4'>
          {
            customerName ?
              <p>Tên khách hàng : <b>{customerName}</b></p>
              :
              null
          }
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <DataOrders aRROrders={aRROrders} phoneCustomer={phoneCustomer} restApiGetOrderByPhone={restApiGetOrderByPhone} restApiGetAllOrders={restApiGetAllOrders} />
      </Row>

    </>
  )
}
export default OrdersComponent
