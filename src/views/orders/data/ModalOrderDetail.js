import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap"
import { Button, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, TextField } from "@mui/material"
import axios from 'axios'
import { useEffect, useState, React } from "react"
const ModalOrderDetail = ({ openModalOrderDetail, setOpenModalDetail, orderId }) => {
    // Chỉnh style cho table giỏ hàng
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));
    const [aRROrderDetailById, setArrOrderDetailById] = useState([])

    let sumPrice = 0
    let sumAmount = 0
    let priceProduct = 0

    for (let i in aRROrderDetailById) {
        priceProduct = aRROrderDetailById[i].PriceEach
        sumAmount += aRROrderDetailById[i].Quantity
        sumPrice += priceProduct
    }
    const restApiGetOrderDetailById = async () => {
        axios.get(`http://computer-tech-be.herokuapp.com/orders/${orderId}/orderDetail`)
            .then((data) => {
                setArrOrderDetailById(data.data.Order)
            })
            .catch((error) => {
                console.log(error.response);
            })
    }

    useEffect(() => {
        restApiGetOrderDetailById()
    }, [orderId])
    return (
        <>
            <Modal isOpen={openModalOrderDetail} toggle={() => setOpenModalDetail(false)} size='lg'>
                <ModalHeader>Chi tiết</ModalHeader>
                <ModalBody>
                    <TableContainer >
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow style={{ color: 'white', backgroundColor: 'darkgray' }}>
                                    <TableCell width={'5%'} align="center"><h5>STT</h5></TableCell>
                                    <TableCell width={'20%'} align="center"><h5>Hình</h5></TableCell>
                                    <TableCell width={'20%'} align="center"><h5>Tên sản phẩm</h5></TableCell>
                                    <TableCell width={'10%'} align="center"><h5>Số lượng</h5></TableCell>
                                    <TableCell width={'15%'} align="center"><h5>Giá</h5></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    aRROrderDetailById.map((item, index) => {
                                        return (
                                            <StyledTableRow key={index}>
                                                <StyledTableCell align="center">{index + 1}</StyledTableCell>
                                                <StyledTableCell align="center"><img src={item.ImgUrl} style={{ width: '50%' }} alt='imgUrl' /></StyledTableCell>
                                                <StyledTableCell align="center">{item.Name}</StyledTableCell>
                                                <StyledTableCell align="center">{item.Quantity}</StyledTableCell>
                                                <StyledTableCell align="center">{item.PriceEach.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} VNĐ</StyledTableCell>
                                            </StyledTableRow >
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </ModalBody>
                <ModalFooter>
                    <p>
                        <h3>Tổng thanh toán ( {sumAmount} sản phẩm): </h3>
                    </p>
                    <h1 style={{ color: 'red' }}>{sumPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} VNĐ</h1>

                </ModalFooter>
            </Modal>
        </>
    )
}
export default ModalOrderDetail
