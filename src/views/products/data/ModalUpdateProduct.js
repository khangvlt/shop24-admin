import { MenuItem, Select, TextField, Button, Snackbar, Alert } from "@mui/material"
import axios from "axios"
import { useEffect, useState, React } from "react"
import { Label, Modal, ModalBody, ModalHeader, Row, Col, ModalFooter } from "reactstrap"

const ModalUpdateProduct = ({ openModalUpdate, setOpenModalUpdate, productId, restAPIGetAllProducts }) => {
  const [buyPrice, setBuyPrice] = useState(0)
  const [promotionPrice, setPromotionPrice] = useState(0)
  const [brand, setBrand] = useState(null)
  const [name, setName] = useState(null)
  const [type, setType] = useState(null)
  const [quantity, setQuantity] = useState(null)


  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const setAlert = (paramModal, paramAlert) => {
    setOpenAlert(true)
    setStatusModal(paramModal)
    setNoidungAlertValid(paramAlert);
  }

  const restApiGetProductById = () => {
    axios.get(`http://computer-tech-be.herokuapp.com/products/${productId}`)
      .then((data) => {
        setBuyPrice(data.data.product.BuyPrice)
        setPromotionPrice(data.data.product.PromotionPrice)
        setBrand(data.data.product.Brand)
        setName(data.data.product.Name)
        setType(data.data.product.Type)
        setQuantity(data.data.product.Quantity)
      })
      .catch((err) => {
        console.log(err.message)
      })
  }

  const onChangeBuyPrice = (event) => {
    setBuyPrice(event.target.value)
  }
  const onChangePromotionPrice = (event) => {
    setPromotionPrice(event.target.value)
  }
  const onChangeBrand = (event) => {
    setBrand(event.target.value)
  }
  const onChangeQuantity = (event) => {
    setQuantity(event.target.value)
  }

  const updateProductById = () => {
    const body = {
      body: {
        BuyPrice: buyPrice,
        PromotionPrice: promotionPrice,
        Brand: brand,
        Quantity: quantity,
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.put(`http://computer-tech-be.herokuapp.com/products/${productId}`, body.body, body.headers)
      .then(() => {
        setAlert('success', 'Cập nhật thành công')
        restAPIGetAllProducts()
      })
      .catch(error => {
        setAlert('error', 'Cập nhật thất bại')
      })
  }
  const onClickUpdateProduct = () => {
    updateProductById()
  }

  useEffect(() => {
    restApiGetProductById()
  }, [productId])
  return (
    <>
      <Modal isOpen={openModalUpdate} toggle={() => setOpenModalUpdate(false)} size='lg'>
        <ModalHeader>Cập nhật sản phẩm</ModalHeader>
        <ModalBody>
          <Row className="p-2 mt-2">
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Tên</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" value={name} style={{ minWidth: 200 }}></TextField></Col>
              </Row>
            </Col>
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Loại</Label></Col>
                <Col xs='9'>
                  <Select value={type} variant="standard" readOnly={true} style={{ minWidth: 200 }}>
                    <MenuItem value='keyboard'>Bàn phím</MenuItem>
                    <MenuItem value='mouse'>Chuột</MenuItem>
                    <MenuItem value='monitor'>Màn hình</MenuItem>
                    <MenuItem value='vga'>VGA</MenuItem>
                    <MenuItem value='ssd'>SSD</MenuItem>
                    <MenuItem value='case'>Case</MenuItem>
                    <MenuItem value='headphone'>Tai nghe</MenuItem>
                    <MenuItem value='mainboard'>Mainboard</MenuItem>
                  </Select>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="p-2 mt-2">
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Giá mua</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" onChange={onChangeBuyPrice} value={buyPrice} style={{ minWidth: 200 }}></TextField></Col>
              </Row>
            </Col>
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Giá giảm</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" onChange={onChangePromotionPrice} value={promotionPrice} style={{ minWidth: 200 }}></TextField></Col>
              </Row>
            </Col>
          </Row>
          <Row className="p-2 mt-2">
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Brand</Label></Col>
                <Col xs='9'>
                  <Select value={brand} onChange={onChangeBrand} variant="standard" style={{ minWidth: 200 }}>
                    <MenuItem value='hot'>Hot</MenuItem>
                    <MenuItem value='new'>New</MenuItem>
                  </Select>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="p-2 mt-2">
                <Col xs='3'>
                  <Label>Số lượng</Label></Col>
                <Col xs='9'>
                  <TextField variant="standard" onChange={onChangeQuantity} value={quantity} style={{ minWidth: 200 }}></TextField>
                </Col>
              </Row>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button variant="contained" onClick={onClickUpdateProduct}>Cập nhật</Button>
        </ModalFooter>
      </Modal>
      <Snackbar open={openAlert} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalUpdateProduct
