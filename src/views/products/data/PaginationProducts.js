import { Pagination } from '@mui/material'
import { React, useEffect } from 'react'
const PaginationProducts = ({ page, noPage, setPage }) => {
  const onChangePage = (event, value) => {
    setPage(value)
  }
  return (
    <>
      <Pagination onChange={onChangePage}
        size='large'
        variant='outlined'
        shape="rounded"
        count={noPage}
        defaultPage={page}></Pagination>
    </>
  )
}
export default PaginationProducts
