import { MenuItem, Select, TextField, Button, Snackbar, Alert } from "@mui/material"
import axios from "axios"
import { useEffect, useState, React } from "react"
import { Link } from "react-router-dom"
import { Label, Row, Col } from "reactstrap"
import { FaTrashAlt } from "react-icons/fa";

const CreateProductComponent = () => {

  const [openAlert, setOpenAlert] = useState(false)
  const [noidungAlertValid, setNoidungAlertValid] = useState('')
  const [statusModal, setStatusModal] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const setAlert = (paramModal, paramAlert) => {
    setOpenAlert(true)
    setStatusModal(paramModal)
    setNoidungAlertValid(paramAlert);
  }

  const [aRRDescription, setArrDescription] = useState([])
  const [description, setDesciption] = useState(null)

  let d = new Date()
  let day = d.getDate();
  let month = d.getMonth() + 1;
  let year = d.getFullYear();

  const onChangeDescription = (event) => {
    setDesciption(event.target.value)
  }
  const onClickAddDescription = () => {
    if (description) {
      setArrDescription([...aRRDescription, description])
      document.getElementById('inputDescription').value = ''
    }
    setDesciption(null)
  }
  const onClickDeleteDescription = (index) => {
    setArrDescription(item => item.filter((aRRDescription, i) => i !== index))
  }

  const [aRRImageDetail, setArrImageDetail] = useState([])
  const [imageDetail, setImageDetail] = useState(null)

  const onChangeImageDetail = (event) => {
    setImageDetail(event.target.value)
  }
  const onClickAddImageDetail = () => {
    if (imageDetail && (aRRImageDetail.includes(imageDetail) == false)) {
      setArrImageDetail([...aRRImageDetail, imageDetail])
      document.getElementById('inputImage').value = ''
    }
    else if (aRRImageDetail.includes(imageDetail) == true) {
      setAlert('error', 'Đã có hình này')
    }
    setImageDetail(null)
  }
  const onClickDeleteImageDetail = (index) => {
    setArrImageDetail(item => item.filter((aRRImageDetail, i) => i !== index))
  }

  const [nameProduct, setNameProduct] = useState(null)
  const [typeProduct, setTypeProduct] = useState(null)
  const [buyPrice, setBuyPrice] = useState(null)
  const [promotionPrice, setPromotionPrice] = useState(null)
  const [imgAvatar, setImageAvatar] = useState(null)
  const [brandProduct, setBrandProduct] = useState(null)
  const [quantityProduct, setQuantityProduct] = useState(null)

  const onChangeNameProduct = (event) => {
    setNameProduct(event.target.value)
  }

  const onChangeTypeProduct = (event) => {
    setTypeProduct(event.target.value)
  }

  const onChangeBuyPrice = (event) => {
    setBuyPrice(event.target.value)
  }

  const onChangePromotionPrice = (event) => {
    setPromotionPrice(event.target.value)
  }

  const onChangeImageAvatar = (event) => {
    setImageAvatar(event.target.value)
  }

  const onChangeBrandProduct = (event) => {
    setBrandProduct(event.target.value)
  }

  const onChangeQuantityProduct = (event) => {
    setQuantityProduct(event.target.value)
  }

  const validateData = () => {
    if (!nameProduct) {
      setAlert('error', 'Bạn chưa nhập tên sản phẩm')
      return false
    }
    if (!typeProduct) {
      setAlert('error', 'Bạn chưa chọn loại sản phẩm')
      return false
    }

    if (!buyPrice) {
      setAlert('error', 'Bạn chưa nhập giá mua')
      return false
    }
    if (isNaN(buyPrice)) {
      setAlert('error', 'Giá phải là số')
      return false
    }
    if (promotionPrice) {
      if (isNaN(promotionPrice)) {
        setAlert('error', 'Giá phải là số')
        return false
      }
    }
    if (!imgAvatar) {
      setAlert('error', 'Bạn chưa nhập hình ảnh chính')
      return false
    }
    if (!brandProduct) {
      setAlert('error', 'Bạn chưa chọn Brand')
      return false
    }
    if (!quantityProduct) {
      setAlert('error', 'Bạn chưa nhập số lượng')
      return false
    }
    if (isNaN(quantityProduct)) {
      setAlert('error', 'Số lượng phải là số')
      return false
    }
    if (aRRDescription.length < 1) {
      setAlert('error', 'Bạn phải nhập ít nhất 1 mô tả')
      return false
    }
    return true
  }

  const restApiCreateNewProduct = () => {
    const body = {
      body: {
        Name: nameProduct,
        Type: typeProduct,
        ImageUrl: imgAvatar,
        BuyPrice: buyPrice,
        PromotionPrice: promotionPrice,
        Brand: brandProduct,
        Quantity: quantityProduct,
        Description: aRRDescription,
        ImageArr: aRRImageDetail,
        TimeCreated: `${day}/${month}/${year}`,
        TimeUpdated: `${day}/${month}/${year}`
      },
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    axios.post('http://computer-tech-be.herokuapp.com/products', body.body, body.headers)
      .then(() => {
        setAlert('success', 'Thêm mới sản phẩm thành công');
        document.getElementById('name').value = ''
        document.getElementById('buyprice').value = ''
        document.getElementById('promotionprice').value = ''
        document.getElementById('imgavatar').value = ''
        document.getElementById('selectbrand').value = ''
        document.getElementById('quantity').value = ''
        document.getElementById('selectype').value = ''
        setArrImageDetail([])
        setArrDescription([])
        setImageAvatar(null)
        setBrandProduct(null)
        setTypeProduct(null)
      })
      .catch(error => {
        setAlert('error', 'Thêm mới sản phẩm thất bại!!!');
        console.log(error.response);
      })
  }

  const onClickAddNewProduct = () => {
    let validata = validateData()
    if (validata) {
      restApiCreateNewProduct()
    }
  }
  return (
    <>
      <Row className="mt-2 p-2 text-center">
        <h2>Thêm mới sản phẩm</h2>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Tên sản phẩm</Label>
            </Col>
            <Col xs='8'>
              <TextField id='name' variant="standard" onChange={onChangeNameProduct}></TextField>
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Loại</Label>
            </Col>
            <Col xs='8'>
              <Select variant="standard" id='selectype' value={typeProduct} onChange={onChangeTypeProduct}>
                <MenuItem value='keyboard'>Bàn phím</MenuItem>
                <MenuItem value='mouse'>Chuột</MenuItem>
                <MenuItem value='monitor'>Màn hình</MenuItem>
                <MenuItem value='vga'>VGA</MenuItem>
                <MenuItem value='ssd'>SSD</MenuItem>
                <MenuItem value='headphone'>Tai nghe</MenuItem>
                <MenuItem value='case'>Case</MenuItem>
                <MenuItem value='mainboard'>Mainboard</MenuItem>
                <MenuItem value='ram'>Ram</MenuItem>
                <MenuItem value='speaker'>Loa</MenuItem>
                <MenuItem value='laptop'>Laptop</MenuItem>
                <MenuItem value='table'>Bàn/ ghế</MenuItem>
              </Select>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Giá mua</Label>
            </Col>
            <Col xs='8'>
              <TextField id='buyprice' variant="standard" onChange={onChangeBuyPrice}></TextField>
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Giá giảm</Label>
            </Col>
            <Col xs='8'>
              <TextField id='promotionprice' variant="standard" onChange={onChangePromotionPrice}></TextField>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Hình ảnh</Label>
            </Col>
            <Col xs='8'>
              <TextField id='imgavatar' variant="standard" onChange={onChangeImageAvatar}></TextField>
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Brand</Label>
            </Col>
            <Col xs='8'>
              <Select variant="standard" id='selectbrand' onChange={onChangeBrandProduct} value={brandProduct} >
                <MenuItem value='new'>New</MenuItem>
                <MenuItem value='hot'>Hot</MenuItem>
              </Select>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>

            </Col>
            <Col xs='8'>
              {
                imgAvatar ?
                  <img src={imgAvatar} alt='img' style={{ width: '40%' }} />
                  : null
              }
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='4'>
              <Label>Số lượng</Label>
            </Col>
            <Col xs='8'>
              <TextField id='quantity' variant="standard" onChange={onChangeQuantityProduct}></TextField>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='2'>
              <Label>Mô tả sản phẩm</Label>
            </Col>
            <Col xs='10'>
              <TextField variant="standard" id="inputDescription" onChange={onChangeDescription}></TextField>
              <Button variant="contained" style={{ margin: 5 }} onClick={onClickAddDescription}>Thêm </Button>
            </Col>
          </Row>
          <Row className="mt-2 p-2">
            <Col xs='12'>
              {
                aRRDescription.length > 0 ?
                  aRRDescription.map((item, index) => {
                    return (
                      <Row key={index}>
                        <Col xs='4'>
                          <Label style={{ marginRight: 10 }}><b>Mô tả thứ {index + 1}</b> : {item}</Label>
                        </Col>
                        <Col xs='2'>
                          <FaTrashAlt onClick={() => onClickDeleteDescription(index)} style={{ color: 'red' }} className='styleIcon' />
                        </Col>
                      </Row>
                    )
                  })
                  : null
              }</Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Col>
          <Row className="mt-2 p-2">
            <Col xs='2'>
              <Label>Hình ảnh chi tiết</Label>
            </Col>
            <Col xs='10'>
              <TextField variant="standard" id='inputImage' onChange={onChangeImageDetail} ></TextField>
              <Button variant="contained" style={{ margin: 5 }} onClick={onClickAddImageDetail}>Thêm </Button>
            </Col>
          </Row>
          <Row className="mt-2 p-2 text-center">
            <Col xs='12'>
              <Row>
                {
                  aRRImageDetail.length > 0 ?
                    aRRImageDetail.map((item, index) => {
                      return (
                        <Col key={index} xs='2'>
                          <img src={item} alt='img' style={{ width: '30%', margin: 5 }} />
                          <FaTrashAlt onClick={() => onClickDeleteImageDetail(index)} className='styleIcon' style={{ color: 'red' }} />
                        </Col>
                      )
                    })
                    : null
                }
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-2 p-2">
        <Button variant="contained" onClick={onClickAddNewProduct}>Thêm sản phẩm</Button>
      </Row>
      <Snackbar open={openAlert} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} autoHideDuration={3000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModal}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default CreateProductComponent
