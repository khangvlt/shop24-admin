import { Button, MenuItem, Select, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, TextField } from "@mui/material"
import { useState, React } from "react";
import ModalOrderDetail from "./ModalOrderDetail";
import ModalUpdateStatus from "./ModalUpdateStatus";
import { Col, Label, Row } from 'reactstrap'
import axios from "axios";

const DataOrders = ({ aRROrders, restApiGetAllOrders, phoneCustomer, restApiGetOrderByPhone }) => {
  // Chỉnh style cho table giỏ hàng
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const [openModalOrderDetail, setOpenModalDetail] = useState(false)
  const [openModalUpdateStatus, setOpenModalUpdateStatus] = useState(false)
  const [orderId, setOrderId] = useState(null)
  const [status, setStatus] = useState(null)

  const onClickOrderDetail = (paramOrderId) => {
    setOpenModalDetail(true)
    setOrderId(paramOrderId)
  }

  const onChangeStatusOrder = (paramOrderId, event) => {
    setOpenModalUpdateStatus(true)
    setOrderId(paramOrderId)
    setStatus(event.target.value)
  }


  return (
    <>
      <Row className='mt-2 p-2'>
        <TableContainer >
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow style={{ color: 'white' }} className="bg-info">
                <TableCell width={'5%'} align='center'><h5>STT</h5></TableCell>
                <TableCell width={'20%'} align="center"><h5>Mã đơn hàng</h5></TableCell>
                <TableCell width={'20%'} align="center"><h5>Ngày đặt hàng</h5></TableCell>
                <TableCell width={'20%'} align="center"><h5>Ngày giao hàng</h5></TableCell>
                <TableCell width={'10%'} align="center"><h5>Trạng thái</h5></TableCell>
                <TableCell width={'15%'} align="center"><h5>Chi tiết</h5></TableCell>
                <TableCell width={'25%'} align="center"><h5>Cập nhật đơn hàng</h5></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                aRROrders.map((item, index) => {
                  return (
                    <StyledTableRow key={index}>
                      <StyledTableCell width={'5%'} align='center'>{index + 1}</StyledTableCell>
                      <StyledTableCell width={'20%'} align='center'>{item._id}</StyledTableCell>
                      <StyledTableCell width={'20%'} align='center'>{item.OrderDate}</StyledTableCell>
                      <StyledTableCell width={'20%'} align='center'>{item.ShippedDate}</StyledTableCell>
                      <StyledTableCell width={'10%'} align='center'>{item.Status}</StyledTableCell>
                      <StyledTableCell width={'15%'} align='center'>
                        <Button variant="contained" onClick={() => onClickOrderDetail(item._id)}>Chi tiết</Button>
                      </StyledTableCell>
                      <StyledTableCell width={'25%'} align='center'>
                        <Select variant="standard" onChange={(e) => onChangeStatusOrder(item._id, e)} style={{ minWidth: 100 }}>
                          <MenuItem value='confirmed'>Confirmed</MenuItem>
                          <MenuItem value='cancel'>Cancel</MenuItem>
                        </Select>
                      </StyledTableCell>
                    </StyledTableRow>
                  )
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Row>
      <ModalOrderDetail orderId={orderId} openModalOrderDetail={openModalOrderDetail} setOpenModalDetail={setOpenModalDetail} />
      <ModalUpdateStatus phoneCustomer={phoneCustomer} restApiGetOrderByPhone={restApiGetOrderByPhone} orderId={orderId} restApiGetAllOrders={restApiGetAllOrders} status={status} openModalUpdateStatus={openModalUpdateStatus} setOpenModalUpdateStatus={setOpenModalUpdateStatus} />
    </>
  )
}
export default DataOrders
